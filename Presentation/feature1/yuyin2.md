

# Front-end features

\begin{center}
\Huge Front-end features for spoofing detection - Static + Dynamic investigation
\end{center}

# Topics Discussed

* Paper discusses the usage of **static**, **delta** and **delta-delta** features for spoofing detection
* ASVSpoof2015 and BTAS2016 results are shown

# Framework

![Framework](feature1/index_files/15114929613802-1.jpg "framework")


# Feature Types

* Mel cepstrum coefficients (MFCC)
* Constant Q-Transform coefficients (CQCC)

# MFCC
![MFCC](feature1/index_files/1511493168487mfcc.jpg "MFCC Feature extraction")

# CQCC
![CQCC](feature1/index_files/cqcc.jpg "CQCC Feature extraction")


# Experiment Setup
* Database Description: ASVspoof 2015, BTAS 2016
* Feature Extraction Techniques: MFCCs, CQCCs
* Static : No delta feature included, Dynamic: Delta and Delta-Delta features
* Classifier and Performance Evaluation: GMM-ML classifier

# BTAS2016 and ASVSpoof2015

* ASVSpoof2015 - Focus on **Synthesis** and **VC**. There is no "replay" component in here, it is assumed that the system has been bypassed. Traindatalength `= 30h`. 
* BTAS2016 - Focus on the same as above but with realistic replay of VC and SS to a target system.

|   Dataset    | Trainduration (h) | Evalduration (h) |
|--------------|-------------------|------------------|
| BTAS2016     |                51 |               52 |
| ASVSpoof2015 |                16 |              170 |



# Results and Analysis (BTAS2016)

<!-- **BTAS2016:** the performance degrades drastically when one attack type is excluded from training the models and when the system is confronted with the similar type of attack in the system assessment process. SS and VC attacks are different whereas replay attacks have a high similarity with genuine speech.
 -->

![BTAS Results](feature1/index_files/2-2.jpg "btas2016")



# Results and Analysis (ASVSpoof2015)


**ASVspoof2015:** the dynamic coefficients provide superior performance in detecting synthetic spoofed signals. CQCC feature leads to better performance across all cases of generalization scenarios.

![ASVSpoof Results](feature1/index_files/asvspoof2015.png "asvspoof2015")

# Conclusion

* Empiric feature studies for each dataset are necessary!
* Each feature needs to be analyzed for static - dynamic usage. Static features work well on Replay based - BTAS2016, while dynamic ones work well on the synthesized one.
* "Good" features are currently only task-based.

