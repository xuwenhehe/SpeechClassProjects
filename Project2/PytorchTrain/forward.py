# -*- coding: utf-8 -*-
# @Author: Heinrich Dinkel
# @Date:   2017-12-12 21:01:19
# @Last Modified by:   richman
# @Last Modified time: 2017-12-26 10:18:02
import argparse
import numpy as np
import torch
import torchnet as tnt
import torch.nn.functional as F
from torch.autograd import Variable
import models
import shelve
from sklearn.preprocessing import StandardScaler, robust_scale, MinMaxScaler

from sklearn.feature_extraction import image


parser = argparse.ArgumentParser()
""" Arguments: evaldata,cvdata """
parser.add_argument('model', type=argparse.FileType('r'), help="Trained model")
parser.add_argument('evaldata', type=shelve.open,
                    help="Stream, e.g. copy-feats scp:feats ark:- |")
parser.add_argument('-nthreads', type=int, default=4,
                    help="Number of threads. Default is %(default)s")
parser.add_argument('-usegpu', default=False, action='store_true')
parser.add_argument('-spk', default=0, type=int,
                    help="Output speaker. Taking spk num: %(default)s.")
parser.add_argument('-fext', default=5, type=int)
args = parser.parse_args()


torch.set_num_threads(args.nthreads)

# Lambda storage assures that the loaded model will be on CPU
checkpoint = torch.load(
    args.model.name, map_location=lambda storage, loc: storage)

model = checkpoint['model']
fext = 2 * args.fext + 1
model.eval()


scaler = checkpoint['scaler']

if args.usegpu:
    model = model.cuda()
for k, data in args.evaldata.items():
    # Normalize
    if scaler:
        data = scaler.transform(data)
    # Frame extension
    paddeddata = np.empty(
        (data.shape[0] + fext * 2, data.shape[1]), dtype=np.float32)
    paddeddata[:fext, :] = data[0]
    paddeddata[-fext:, :] = data[-1]
    paddeddata[fext:-fext, :] = data
    patches = image.extract_patches_2d(
        paddeddata, (fext, paddeddata.shape[-1])).reshape(-1, fext * paddeddata.shape[-1])
    v = torch.from_numpy(patches).float()
    outputs = []
    batches = torch.utils.data.DataLoader(tnt.dataset.TensorDataset(
        v), batch_size=256, drop_last=False, shuffle=False)
    for batch in batches:
        if args.usegpu:
            batch = batch.cuda()
        out = model(Variable(batch, volatile=True)).data.float()
        outputs.append(out)
    output = Variable(torch.cat(outputs), volatile=True)
    lld = F.softmax(output, dim=1).data.mean(dim=0)
    print(k, lld[args.spk])
